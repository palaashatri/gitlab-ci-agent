# gitlab-ci-agent

[Docker-in-Docker (DinD)](https://www.docker.com/blog/docker-can-now-run-within-docker/)  image configured with [OpenJDK 11](https://pkgs.alpinelinux.org/flag/community/openjdk11/11.0.22_p7-r0) and [Maven](https://pkgs.alpinelinux.org/package/v3.19/community/x86_64/maven) to allow use with [`com.spotify.dockerfile-maven-plugin`](https://mvnrepository.com/artifact/com.spotify/dockerfile-maven-plugin) to build Docker images with commands such as `mvn clean install` in Gitlab CI/CD pipelines.

Reference: [Use Docker to build Docker images - Docker-in-Docker](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker)

## Usage

- Inside your `.gitlab-ci.yml` file, during when configuring `build` stage, use this:

```yml

build:
  stage: build
  image: palaasha/gitlab-ci-agent:latest
  services:     
    - name: docker:dind
      alias: docker
  before_script:
      - echo "Run pre-stage commands here, like setting up your .m2 settings ... "
  script:
    - echo "Use your maven commands here ... "
    - mvn clean install
  after_script:
    - echo "cleanup .m2 cache repo .."
    - rm -rf ~/.m2

```