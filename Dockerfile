FROM docker:dind
# INSTALL JAVA AND MAVEN
RUN apk --no-cache add openjdk11=11.0.22_p7-r0 maven curl
# INSTALL DOCKER-COMPOSE
RUN curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose 
RUN chmod +x /usr/local/bin/docker-compose

VOLUME /var/lib/docker
EXPOSE 2375 2376

LABEL org.opencontainers.image.authors="Palaash Atri (palaash.atri@gmail.com)"
CMD []
